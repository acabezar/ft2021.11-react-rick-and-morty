import { useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header/Header';
import { ProfileContext } from './contexts/ProfileContext';
import CharactersPage from './pages/CharactersPage/CharactersPage';
import CharactersDetailPage from './pages/CharactersPage/CharactersDetailPage/CharactersDetailPage';
import HomePage from './pages/HomePage/HomePage';


const profileDefault = {
  "name": "Rick Sanchez",
  "status": "Alive",
  "species": "Human",
  "image": "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
}

function App() {

  const [profile, setProfile] = useState(profileDefault);

  return (
    <ProfileContext.Provider value={{ profile, setProfile }}>
      <BrowserRouter>
        <Header />
        <div className="container">
          Pepe

          <Routes>
            {/* <Route path="/">
          <Route index element={<HomePage />} />
          <Route path="*" element={<ErrorsPage />} />
          <Route path="characters" element={<CharactersPage />} />
        </Route> */}

            <Route path="/" element={<HomePage />} />
            <Route path="/characters" element={<CharactersPage />} />
            <Route path="/characters/:idCharacter" element={<CharactersDetailPage />} />

          </Routes>
        </div>
      </BrowserRouter>
    </ProfileContext.Provider>
  );
}

export default App;
