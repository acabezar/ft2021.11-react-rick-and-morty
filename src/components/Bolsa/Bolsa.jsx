import { useCallback, useEffect, useState } from "react"

export default function Bolsa(){
    const [euro, setEuro] = useState(10);
    const [dollar, setDollar] = useState(1);

    const allChange = useMemo(() => {
        console.log("hey!");
    }, []);

    // const changeEuro = useCallback((externalEuro) =>{
    //     setEuro(externalEuro + 1)
    // }, []);

    const changeEuro = useCallback((externalEuro) =>{
        setEuro(externalEuro + 1)
    }, []);

    // const changeEuro = useCallback(() =>{
    //     setEuro(euro + 1)
    // }, [euro]);

    const changeDollar = useCallback(() =>{
        setDollar(dollar + 1)
    }, [dollar]);

    useEffect(() => {
        setInterval(() => {
            changeEuro(euro)
        }, 1000)
    }, [])

    return <div>
        {euro}
        {dollar}
    </div>
}