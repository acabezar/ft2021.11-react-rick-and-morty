import { useContext } from "react"
import { CounterContext } from "../../contexts/CounterContext";

export default function Counter(){

    const {counter, setCounter} = useContext(CounterContext);
    return <div>
        Soy un contador
        {counter}

        <button onClick={() => setCounter(counter + 1)}>+</button>
    </div>
}