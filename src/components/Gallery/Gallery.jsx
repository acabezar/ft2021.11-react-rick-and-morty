import { useContext } from "react"
import { Link } from "react-router-dom";
import { ProfileContext } from "../../contexts/ProfileContext"

export default function Gallery({ list }) {
    const { setProfile } = useContext(ProfileContext);


    const changeProfile = (newProfile) => {
        setProfile(newProfile);
        document.body.classList.add("rotate-center")
        document.body.setAttribute("theme", newProfile.species.toLowerCase())

        setTimeout(() => {
            document.body.classList.remove("rotate-center")
        }, 600)
    }

    return <div className="row">
        {list.map(item => <figure className="col-12 col-sm-6 col-md-4 col-lg-3" key={item.id}>
            <Link to={"/characters/" + item.id}>
                <img data-testid={"gallery-img-" + item.id} src={item.image} alt={item.name} />
                <figcaption data-testid={"gallery-text-" + item.id} >
                    {item.name} <button  data-testid={"gallery-button-" + item.id} className="b-btn b-btn--sec" onClick={() => changeProfile(item)}>Mutar</button>
                </figcaption>
            </Link>
        </figure>)}
    </div>
}