import { fireEvent, prettyDOM, render, screen } from '@testing-library/react';
import { useState } from 'react';
import { MemoryRouter } from 'react-router-dom';
import { ProfileContext } from '../../contexts/ProfileContext';
import Gallery from './Gallery';


const characters = [
    {
        "id": 1,
        "name": "Rick Sanchez",
        "status": "Alive",
        "species": "Human",
        "type": "",
        "gender": "Male",
        "origin": {
            "name": "Earth (C-137)",
            "url": "https://rickandmortyapi.com/api/location/1"
        },
        "location": {
            "name": "Citadel of Ricks",
            "url": "https://rickandmortyapi.com/api/location/3"
        },
        "image": "https://rickandmortyapi.com/api/character/avatar/1.jpeg",

        "url": "https://rickandmortyapi.com/api/character/1",
        "created": "2017-11-04T18:48:46.250Z"
    }
]

test('render', () => {
    render(<MemoryRouter><Gallery list={characters} /></MemoryRouter>);

    const img = screen.getByTestId("gallery-img-1");
    const text = screen.getByTestId("gallery-text-1");

    expect(img).toHaveAttribute("src", "https://rickandmortyapi.com/api/character/avatar/1.jpeg");
    expect(text).toHaveTextContent("Rick Sanchez");
});

test('click mutar', () => {
    const profile = {
        "name": "Rick Sanchez",
        "status": "Alive",
        "species": "Human",
        "image": "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
    }

    const setProfile = jest.fn();

    render(<MemoryRouter>
        <ProfileContext.Provider value={{ profile, setProfile }}>
            <Gallery list={characters} />
        </ProfileContext.Provider>
    </MemoryRouter>);

    const button = screen.getByTestId("gallery-button-1");

    fireEvent.click(button)

    // console.log(prettyDOM());

    expect(document.body).toHaveAttribute("theme", "human")
    expect(document.body).toHaveAttribute("class", "rotate-center")
})
