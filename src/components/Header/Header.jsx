import { NavLink } from "react-router-dom";
import "./Header.scss";

import Profile from "../Profile/Profile";


export default function Header(){
    return <header className="mb-3 c-header d-none d-lg-flex justify-content-between">
        <nav className="d-flex justify-content-center">
            <NavLink to="/" className="c-header__link">Home</NavLink>
            <NavLink to="/characters" className="c-header__link">Characters</NavLink>
        </nav>
        <Profile/>
    </header>
}