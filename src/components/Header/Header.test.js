import { render, screen } from "@testing-library/react"
import { MemoryRouter } from "react-router-dom"
import { ProfileContext } from "../../contexts/ProfileContext"
import Header from "./Header"


test("render", () => {
    const profile = {
        "name": "Rick Sanchez",
        "status": "Alive",
        "species": "Human",
        "image": "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
    }

    render(<MemoryRouter>
        <ProfileContext.Provider value={{profile}}>
            <Header />
        </ProfileContext.Provider>
    </MemoryRouter>)


    const link = screen.getByText(/home/i);
    const link2 = screen.getByText(/characters/i);

    expect(link).toBeInTheDocument()
    expect(link2).toBeInTheDocument()
})