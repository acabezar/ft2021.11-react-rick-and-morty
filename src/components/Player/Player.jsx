import { useRef } from "react";

export default function Player () {

    const audioRef = useRef();

    const play = () => {
        // const audio$$ = document.querySelector("audio");
        // audio$$.play();

        audioRef.current.play();
    }

    return <div>
        <button onClick={play}>Play audio</button>
        <audio ref={audioRef} src="pepe.mp3"></audio>
    </div>
}