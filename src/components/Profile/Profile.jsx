import { useContext } from "react"
import { ProfileContext } from "../../contexts/ProfileContext"
import "./Profile.scss";

export default function Profile(){

    const {profile} = useContext(ProfileContext);
   
    return <div className="c-profile">
        
        <img className="c-profile__img" src={profile.image} alt={profile.name} />
        <h4>{profile.name} - {profile.species}</h4>
    </div>
}