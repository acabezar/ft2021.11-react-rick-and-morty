import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function CharactersDetailPage() {
    const { idCharacter } = useParams();

    const [character, setCharacter] = useState({});

    useEffect(() => {
        const getCharacter = async () => {
            const { data } = await axios("https://rickandmortyapi.com/api/character/" + idCharacter);
            setCharacter(data);
        }
        getCharacter();

    }, [])

    return <div>
        <h1>{character.name}</h1>
        <div className="row">
            <div className="col-12 col-lg-4">
                <img src={character.image} alt={character.name}/>
            </div>
        </div>
        <p>Specie: {character.species}</p>
        <p>Gender: {character.gender}</p>
    </div>
}