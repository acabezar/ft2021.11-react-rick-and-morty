import axios from "axios"
import { useEffect, useState } from "react"
import Gallery from "../../components/Gallery/Gallery";
import Pagination from "../../components/Pagination/Pagination";
import { Dropdown } from 'primereact/dropdown';
import Swiper, { Navigation, Pagination as SwiperPagination} from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';


const citySelectItems = [
    {label: 'New York', value: 'NY'},
    {label: 'Rome', value: 'RM'},
    {label: 'London', value: 'LDN'},
    {label: 'Istanbul', value: 'IST'},
    {label: 'Paris', value: 'PRS'}
];

export default function CharactersPage() {
    const [characters, setCharacters] = useState([]);
    const [city, setCity] = useState("");

    const getCharacters = async (newPage = 1) => {
        const res = await axios("https://rickandmortyapi.com/api/character?page=" + newPage);
        setCharacters(res.data.results);
        createSlider()
    }

    const createSlider = () => {
        const swiper = new Swiper('.g-swiper', {
            modules: [Navigation, SwiperPagination],
            pagination: {
                el: '.swiper-pagination',
            },

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }

    useEffect(() => {
        getCharacters();
        createSlider()

    }, [])

    return <div>
        <div className="g-swiper swiper">
            <div className="swiper-wrapper">
                {characters.map(character => <div key={character.id} className="swiper-slide"><img src={character.image} alt={character.name}/></div>)}
            </div>
            <div className="swiper-pagination"/>

            <div className="swiper-button-prev"/>
            <div className="swiper-button-next"/>
        </div>


        <Dropdown className="g-primereact-dropdown" value={city} options={citySelectItems} onChange={(e) => setCity(e.value)} placeholder="Select a City"/>
        <Dropdown className="g-primereact-dropdown" value={city} options={characters.map(character => ({label: character.name, value: character.name}))} onChange={(e) => setCity(e.value)} placeholder="Select a City"/>

        <button className="b-btn">Hola</button>

        <Gallery list={characters} />
        <Pagination getData={getCharacters} />

    </div>
}
