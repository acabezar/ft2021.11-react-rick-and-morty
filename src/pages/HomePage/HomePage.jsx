import React, { useState } from "react";
import Counter from "../../components/Counter/Counter";
import { CounterContext } from "../../contexts/CounterContext";
import { Button } from 'primereact/button';


export default function HomePage() {
    const [counter, setCounter] = useState(1);

    return <div>
        <Button className="b-btn" label="Save" />
        soy home
        <button className="b-btn">Hola</button>
        <CounterContext.Provider value={{counter, setCounter}}>

            <Counter />
            <Counter />
            <Counter />
            <Counter />

            {/* <Pepe/> */}

        </CounterContext.Provider>

        <Counter />

    </div>
}
